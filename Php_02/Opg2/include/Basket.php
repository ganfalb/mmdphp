<?php

    /*
    *   I denne fil er placeret funktioner, der har til formål, at udføre handlinger i relation til indkøbskurven. fx placere varere i kurven, tælle varer i kurven, m.v.
    *
    */
    class basket
    {
        public $basket = array(
            array('ProductId'=>'1', 'ProductName'=>'Æbler', 'ProductDescription'=>'Dejlige æbler', 'ProductPrice'=>'5.5'),
            array('ProductId'=>'2', 'ProductName'=>'Appelsiner', 'ProductDescription'=>'Dejlige appelsiner', 'ProductPrice'=>'5.5'),
            array('ProductId'=>'3', 'ProductName'=>'Pære', 'ProductDescription'=>'Dejlige pære', 'ProductPrice'=>'5.5'));
        //Funktion der tilføjer et produkt til kurven

        function __construct(){
            var_dump($this->basket);
        }
        public function addProductToBasket(array $product)
        {
            $watermelon = array("productId"=>"4", "ProductName"=>"vandmelon", "productDescription"=>"Dejlig vandmelon", "ProductPrice"=>"5.6");
            array_push( $this->basket, $watermelon);

            return $this->basket;
        }

        function removeProductFromBasket($productId)
        {
            $i = 0;
            foreach ($this->basket as $product) {
                foreach ($product as $key => $value) {
                    if ($key == "ProductId" AND $value == $productId ) {
                        //Splice benytter vi til, at fjerne en eller flere elementer og eventuelt erstatte med et nyt.
                        array_splice($this->basket, $i, 1);
                    }
                }
                $i++;
            }
            
            return $this->basket;
        }

        function returnQuantityInBasket()
        {
            return count($this->basket);
        }

        function returnTotalPriceInBasket()
        {
            $totalPrice = 0;
            foreach ($this->basket as $product) {
                foreach ($product as $key => $value) {
                    if ($key == "ProductPrice") {
                        $totalPrice += $value;
                    }
                }
            }
            return $totalPrice;
        }
    }
?>