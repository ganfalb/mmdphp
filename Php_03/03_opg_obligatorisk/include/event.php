<?php
    /*
     * Opgave 03_01
     * 
     * En event er en begivendhed der er afgrænset ved tid og sted, og som er kendetegnet ved specifikt indhold.
     * 
     * Opgave 1) 
     * Find nogle flere events. Søg inspiration på http://www.visitskive.dk/skive/begivenheder-2
     * Indsæt de fundne events i $events arrayet nede i klassen Event. Find latitude og longitude på https://itouchmap.com/latlong.html
     *
     * Opgave 2)
     * I klassen Event er der en tom metode der hedder getAllEvents. Metoden skal returnere hele $events arrayet fra klassen.
     * Når vi returnerer hele arrayet, så kan vi anvende det andre steder. Fx i index.php
     *
     * Opgave 3)
     * Gå til index.php
     * Lokaliser der hvor markører bliver indsat på kortet. Erstat de to eksisterende markører med php kode. Koden skal løbe igennem det array, som metoden getAllEvents 
     * returnerer og udskrive indholdet, således, at hver gang løkken løber en gang, vises en markør på kortet.
     * 
     * 
     */
    
    class Event
    {
        private $events = array(
            array(
            "EventId"=>1,
            "EventName"=>"Rave party",
            "EventDescription"=>"For young people",
            "EventDate"=>"Oktober 1 2016 10:00pm",
            "Lat"=>"56.4",
            "Long"=>"9",
            "EventImage"=>"img/rave.png"
        ),
        array(
            "EventId"=>2,
            "EventName"=>"Opera",
            "EventDescription"=>"For not so young people",
            "EventDate"=>"Oktober 2 2016 10:00pm",
            "Lat"=>"56.3",
            "Long"=>"9.4",
            "EventImage"=>"img/opera.png"
        ),
        array(
            "EventId"=>3,
            "EventName"=>"Metal",
            "EventDescription"=>"For everybody",
            "EventDate"=>"Oktober 2 2016 2:00am",
            "Lat"=>"56.4",
            "Long"=>"9.3",
            "EventImage"=>"img/metal.png"
        ),
        array(
            "EventId"=>4,
            "EventName"=>"Limfjorden Rundt",
            "EventDescription"=>"For alle",
            "EventDate"=>"Oktober 17 2016 8:00am",
            "Lat"=>"56.566136",
            "Long"=>"9.051155",
            "EventImage"=>"img/metal.png"
        ),
        array(
            "EventId"=>5,
            "EventName"=>"Restaurant",
            "EventDescription"=>"God Mad",
            "EventDate"=>"Oktober 3 2016 5:00am",
            "Lat"=>"56.5",
            "Long"=>"9.0",
            "EventImage"=>"img/metal.png"
        ));
        function __construct()
        {
            //Konstruktør  (funktionen) skal ikke benyttes
        }
        
        function singleEvent($EventId) 
        {
            //Kalder et enkelt event
            foreach ($this->events as $ev) {

                $keys = array_keys($ev);
                $value = array_values($ev);
                if($ev["EventId"] == $EventId) { 
                echo "L.marker([" . $value[4] . ", " . $value[5] . "]).addTo(mymap).bindPopup('" . $value[1] . $value[2] . "');";  
                }
        }
        
        
        function getAllEvents($EventId) 
        {
            //Kalder alle events
            foreach ($this->events as $ev) {

                $keys = array_keys($ev);
                $value = array_values($ev); 
                echo "L.marker([" . $value[4] . ", " . $value[5] . "]).addTo(mymap).bindPopup('" . $value[1] . $value[2] . "');";  
                }
                
                
                
            }
        }
    }
?>