<?php
    // Datatyper og variabler. Dette dokument indlejres i det efterfølgende

    //tekststreng (String)
    $firstname = "Frederik";

    //tekststreng (String)

    $lastname = "Frøsig";

    //nummerisk heltal (integer)
    $age = 22;

    //boolean (sandt/falsk)
    $inRelationship = false;

    //tekststreng (String)
    $work = "Sælger";

    //Tekststreng (string)

    $workPlace = "Bilka";

    //array (en række)
    $hobbies = ["Computer","Gaming","Musik"];

?>