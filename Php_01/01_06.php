<!-- Variabler og datatyper -->

<?php
    /* - datatyper
    * - Integer
    * - Float
    */

    $a = 1234; // decimal number
    var_dump($a);

    $a = -123; // a negative number
    var_dump($a);

    $a = 0123; // octal number
    var_dump($a);

    $a = 1.3;
    var_dump($a);

    $large_number = 2147483647;     // int(2147483647)
    var_dump($large_number);

    $large_number = 2147483648;    // float(2147483648)
    var_dump($large_number);

    $million = 1000000;
    $large_number = 50000 * $million;
    var_dump($large_number);       // float(50000000000)

    var_dump(25/7);                // float(3.5714285714286)

    var_dump((int) (25/7));        // int(3)

    var_dump(round(25/7));         // float(4)

?>