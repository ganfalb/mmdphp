<!doctype>

<head>
    <title>Min kontaktside</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <?php include('../include/data.php') ?>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h2>Resume</h2>
                <p>Mit navn er Klaus Nørregaard og jeg arbejder på Erhvervsakademi Dania. Akademiet har tilknyttet 14 undervisere som er fordelt på 4 uddannelser. Disse uddannelser er Datamatiker, Serviceøkonom, Multimediedesigner og produktionsteknolog.</p>
                <h3>Kontaktoplysninger</h3>
                <p>E: <?php echo $email ?></p>
                <p>T: <?php echo $phone ?></p>
            </div>
            <div class="col-md-3">
                <h2><P></P>ræferencer</h2>
                Følgende emner ligger inden for mit præferenceområde
                <ul>
                    <?php
                        foreach ($preferences as $preference) {
                            echo "<li>" . $preference . "</li>";
                        }
                    ?>        
                </ul>
            </div>
            <div class="col-md-3">
                <h2>Portefølje</h2>
                <p>Følgende er et udpluk af mine opgaver</p>
                <ul>                     
                    <?php
                        foreach ($jobs as $employer => $job) {
                            echo "<li>" . $employer . " <span style='color:#aaa;'>(". $job . ")</span>". "</li>";
                        }
                    ?>    
                </ul>
            </div>
            <div class="col-md-3">
                <h2>Komptencer og færdigheder</h2>
                <ul>
                    <?php
                        foreach ($competencies as $comp) {
                            if(is_array($comp)){
                                foreach ($comp as $c) {
                                    echo "<li>" . $c . "</li>";
                                }
                            }else {
                                echo "<li>" . $comp . "</li>";
                            }
                        }
                    ?>    
                </ul>
            </div>
        </div>
    </div>
    <!-- /container -->
</body>